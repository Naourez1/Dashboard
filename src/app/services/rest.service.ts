import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class RestService {

  public usr_url = environment.base_url + "/user/";
  public projet_url = environment.base_url + "/projet/";
  public module_url = environment.base_url + "/modules/";
  public sprint_url = environment.base_url + "/sprints/";
  public tache_url = environment.base_url + "/taches/";
  public team_url = environment.base_url + "/team/";

  constructor(public http: HttpClient) {
  }
//USER///////////////////

  public login(email, pwd) {
    return this.http.post(this.usr_url + "login", {mail:email,password: pwd});

  }
  public accept_user(id){
    return this.http.get(this.usr_url+"accept_user/"+id)
  }
  public refuse_user(id){
    return this.http.get(this.usr_url+"refuse_user/"+id)
  }
  public registre(e){
    return this.http.post(this.usr_url+'adduser',e)
  }
  public delete(id){
    return this.http.delete(this.usr_url+'delete_user/'+id)
  }
  public one_u(id){
    return this.http.get(this.usr_url+"getuser_byid/"+id)
  }
  public picture(id,img){
   return this.http.post(this.usr_url+"add_pic/"+id,img)
  }
  ////////PROJET////////////
  list_projet(){
    return this.http.get(this.projet_url+"list_project")
  }
  one_p(id){
    return this.http.get(this.projet_url+"one_p/"+id)
  }
  add_projet(p){
    return this.http.post(this.projet_url+"add_project",p)
  }
  edit_projet(p,id){
    return this.http.put(this.projet_url+"update_project/"+id,p)
  }
  delete_p(id){
    return this.http.delete(this.projet_url+'delete_project/'+id)

  }
  list_p_by_M(id){
    return this.http.get(this.projet_url+"list_p_by_m/"+id)
  }
  list_p_by_CH(id){
    return this.http.get(this.projet_url+"list_p_by_chef/"+id)

  }
  list_p_by_empl(id){
    return this.http.get(this.projet_url+"list_p_by_employee/"+id)

  }
  affect_chef(idp,idch){
    return this.http.get(this.projet_url+"affect_ch/"+idp+"/"+idch)
  }
  affect_team(idp,idte){
    console.log('im in affectation')
return this.http.get(this.projet_url+"affectation_team/"+idp+"/"+idte)
  }


  ///////////////CHEF////////////////
  list_chefs(){
    return this.http.get(this.usr_url+"chefs_de_projet")
  }
  //////////////managers////////
  list_managers(){
    return this.http.get(this.usr_url+"managers")
  }
  /////////employees////////
  list_employees(){
    return this.http.get(this.usr_url+"employees")
  }
  ////////MODULES////
  add_module(m,id){
    return this.http.post(this.module_url+"add_modules/"+id,m)
  }
  edit_m(id,m){
    return this.http.put(this.module_url+"update_modules/"+id,m)
  }
  delete_m(id,idp){
    return this.http.delete(this.module_url+"delete_module/"+id+'/'+idp)
  }
  one_m(id){
    return this.http.get(this.module_url+"one_modles/"+id)
  }
  ////////////Sprints//////
  add_sprint(s,id){
    return this.http.post(this.sprint_url+"add_sprint/"+id,s)
  }
  edit_s(id,s){
    return this.http.put(this.sprint_url+"update_sprint/"+id,s)
  }
  one_s(id){
    return this.http.get(this.sprint_url+"one_s/"+id)
  }
  delete_s(id,idm){
    return this.http.delete(this.sprint_url+"delete_s/"+id+'/'+idm)
  }
  //////////Taches/////////
  add_tache(t,id){
    return this.http.post(this.tache_url+'add_taches/'+id,t)
  }
  edit_t(id,t){
    console.log('in service',t)
    return this.http.put(this.tache_url+"update_taches/"+id,t)
  }
  delete_t(id,ids){
    return this.http.delete(this.tache_url+"delete_tache/"+id+'/'+ids)
  }
  one_t(id){
    return this.http.get(this.tache_url+"one_t/"+id)
  }
  //////////TEAM//////////
add_team(te){
   return this.http.post(this.team_url+"add_team",te)
}
add_employee(idteam,idemployee){
    return this.http.get(this.team_url+"add_empl/"+idteam+"/"+idemployee)
}





  public filterProject(name: string ) {
  return this.http.get("http://localhost:3000/projet/filterProjet?author=" + name);
}
  public addmanager(u) {
    return this.http.get("http://localhost:3000/manager/add?firstname=" + u.firstname
      + "&lastname=" + u.lastname
      + "&mail=" + u.mail
      + "&password="
      + u.password
      + "&status="
      + u.status
      + "&position="
      + u.position
      + "&activity="
      + u.activity);
  }

  public getallmanager() {
    return this.http.get("http://localhost:3000/manager/all");
  }

  public removemanager(id) {
    return this.http.get("http://localhost:3000/manager/remove?id=" + id);
  }

  public updatemanager(u) {
    return this.http.get("http://localhost:3000/manager/update?id=" + u._id + "&firstname="
      + u.firstname + "&lastname="
      + u.lastname + "&mail="
      + u.mail + "&password="
      + u.password
      + "&status="
      + u.status
      + "&position="
      + u.position
      + "&activity="
      + u.activity);
  }

  public addchef(u) {
    return this.http.get("http://localhost:3000/chefdeprojet/add?firstname=" + u.firstname
      + "&lastname=" + u.lastname
      + "&mail=" + u.mail
      + "&password="
      + u.password
      + "&status="
      + u.status
      + "&position="
      + u.position
      + "&activity="
      + u.activity);
  }

  public getallchef() {
    return this.http.get("http://localhost:3000/chefdeprojet/all");
  }

  public removechef(id) {
    return this.http.get("http://localhost:3000/chefdeprojet/remove?id=" + id);
  }

  public updatechef(u) {
    return this.http.get("http://localhost:3000/chefdeprojet/update?id=" + u._id + "&firstname="
      + u.firstname + "&lastname="
      + u.lastname + "&mail="
      + u.mail + "&password="
      + u.password + "&status="
      + u.status
      + "&position="
      + u.position
      + "&activity="
      + u.activity);
  }

  public ajouterprojet(u) {
    return this.http.get("http://localhost:3000/projet/add?nomprojet="
      + u.nomprojet + "&datedebut="
      + u.datedebut + "&datefin="
      + u.datefin + "&nomclient="
      + u.nomclient + "&nomequipe="
      + u.nomequipe + "&author="
      + u.author + "&priority="
      + u.priority + "&task="
      + u.task);

  }

  public getallprojet() {
    return this.http.get("http://localhost:3000/projet/all");
  }

  public removeprojet(id) {
    return this.http.get("http://localhost:3000/projet/remove?id=" + id);
  }

  public updateprojet(u) {
    return this.http.get("http://localhost:3000/projet/update?id="
      + u._id + "&nomprojet="
      + u.nomprojet + "&datedebut="
      + u.datedebut + "&datefin="
      + u.datefin + "&nomclient="
      + u.nomclient + "&nomequipe="
      + u.nomequipe + "&author="
      + u.author + "&priority="
      + u.priority + "&task="
      + u.task);

  }

  public findbyidlot(id, u) {
    return this.http.get("http://localhost:3000/projet/findbyidlot?id=" + id + "&idc=" + u._id);
  }

  public getalllot(id) {
    return this.http.get("http://localhost:3000/projet/getalllot?id=" + id);
  }

  public ajouterlot(id, u) {
    return this.http.get("http://localhost:3000/Projet/ajouterlot?id="
      + id + "&nomlot="
      + u.nomlot + "&datedebut="
      + u.datedebut + "&datefin="
      + u.datefin );
  }

  public removelot(id, idc) {
    return this.http.get("http://localhost:3000/Projet/supprimerlot?id=" + id + "&idc=" + idc);
  }

  public updatelot(id, u) {
    return this.http.get("http://localhost:3000/Projet/updatelot?id="
      + id + "&idc="
      + u._id + "&nomlot="
      + u.nomlot + "&datedebut="
      + u.datedebut + "&datefin="
      + u.datefin );
  }

  public ajoutertache(id, idc, u) {
    return this.http.get("http://localhost:3000/projet/ajoutertache?id="
      + id + "&idc="
      + idc + "&title="
      + u.title + "&start="
      + u.start + "&end="
      + u.end );
  }

  public removetache(id, idc, idcc) {
    return this.http.get("http://localhost:3000/projet/supprimertache?id=" + id + "&idc=" + idc + "&idcc=" + idcc);
  }

  public updatetache(id, idc, u) {
    return this.http.get("http://localhost:3000/projet/updatetache?id="
      + id + "&idc="
      + idc + "&idcc="
      + u._id + "&title="
      + u.title + "&start="
      + u.start + "&end="
      + u.end );
  }

  public getalltache(id, idc) {
    return this.http.get("http://localhost:3000/projet/getalltache?id=" + id + "&idc=" + idc);
  }

  public findbyname(nomequipe) {
    return this.http.get("http://localhost:3000/projet/findbyname?nomequipe=" + nomequipe);
  }
  public findbyid(u) {
    return this.http.get("http://localhost:3000/projet/findbyid?id=" + u._id);
  }
  public affecterprojet(id, nomprojet) {
    return this.http.get("http://localhost:3000/chefdeprojet/ajouterporjet?id=" + id + "&nomprojet=" + nomprojet );
  }
  public findbyidprojlot(id) {
    return this.http.get("http://localhost:3000/projet/" + id + "/getlotbyidproj/" );
  }
  public findbyidprojtache(id) {
    return this.http.get("http://localhost:3000/projet/" + id + "/gettaskbyidproj/" );
  }
  public ajoutertache1(id, u) {
    return this.http.get("http://localhost:3000/projet/" + id + "/addtache?title=" + u.title
    + "&start=" + u.start
    + "&end=" + u.end
    + "&description=" + u.description
    + "&estimation=" + u.estimation
    + "&priority=" + u.priority );
  }
  public ajouterlot1(id, u) {
    return this.http.get("http://localhost:3000/projet/" + id + "/addlot?nomlot=" + u.nomlot
    + "&datedebut=" + u.datedebut
    + "&datefin=" + u.datefin
    + "&description=" + u.description
    + "&estimation=" + u.estimation
    + "&priority=" + u.priority
    + "&tache_id=" + u.tache_id );
  }
}
