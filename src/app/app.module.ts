import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCardModule, MatFormFieldModule,
   MatCheckboxModule, MatDatepickerModule, MatRadioModule, MatSelectModule } from '@angular/material';
import { NavService} from './services/nav.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SidemenuComponent } from './home/sidemenu/sidemenu.component';
import { FooterComponent } from './home/footer/footer.component';
import { HeaderComponent } from './home/header/header.component';
import { LayoutComponent } from './home/layout/layout.component';
import { LoginComponent } from './login/login.component';
import { GestionDeChefDeProjetComponent } from './gestion-de-chef-de-projet/gestion-de-chef-de-projet.component';
import { GestiondemanagerComponent } from './gestiondemanager/gestiondemanager.component';
import { ConfigurationprojetComponent } from './configurationprojet/configurationprojet.component';
import { LotComponent } from './lot/lot.component';
import { TacheComponent } from './tache/tache.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RestService} from './services/rest.service';
import { OpenComponent } from './open/open.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { PBComponent } from './pb/pb.component';
import { SBComponent } from './sb/sb.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './register/register.component';
import '../polyfills';
import {MatNativeDateModule} from '@angular/material';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {DemoMaterialModule} from '../material-module';
import { DatePipe } from '@angular/common';
import { environment } from '../environments/environment';
import { DetailsComponent } from './details/details.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { OpenedprojectsComponent } from './openedprojects/openedprojects.component';
import { FilterPipe } from './filter.pipe';
import {TableModule} from 'primeng/table';
import {InputTextModule} from 'primeng/inputtext';
import {DataViewModule} from 'primeng/dataview';
import {NgxPaginationModule} from 'ngx-pagination';
import { CommonModule } from '@angular/common';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import {NgbModalModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import { MeetingsComponent } from './meetings/meetings.component';
import { UsersComponent } from './users/users.component';
import {TimeAgoPipe} from 'time-ago-pipe';
import { TaskfromprojComponent } from './taskfromproj/taskfromproj.component';
import { TaskfromsprintComponent } from './taskfromsprint/taskfromsprint.component';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {ChartModule} from 'primeng/chart';
import {FileUploadModule} from 'primeng/fileupload';
import { DetailsMComponent } from './details/details-m/details-m.component';
import { DetailsSComponent } from './details/details-m/details-s/details-s.component';
import { DetailsTComponent } from './details/details-m/details-s/details-t/details-t.component';
import {AuthGuard} from './guard/auth.guard';








@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidemenuComponent,
    FooterComponent,
    HeaderComponent,
    LayoutComponent,
    LoginComponent,
    GestionDeChefDeProjetComponent,
    GestiondemanagerComponent,
    ConfigurationprojetComponent,
    LotComponent,
    TacheComponent,
    OpenComponent,
    MonitoringComponent,
    MeetingsComponent,
    PBComponent,
    SBComponent,
    RegisterComponent,
    DetailsComponent,
    OpenedprojectsComponent,
    FilterPipe,
    UsersComponent,
    TimeAgoPipe,
    TaskfromprojComponent,
    TaskfromsprintComponent,
    DetailsMComponent,
    DetailsSComponent,
    DetailsTComponent,
  ],
  imports: [
    DragDropModule,
    TableModule,
    NgxPaginationModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatRadioModule,
    MatSelectModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    InputTextModule,
    DataViewModule,
    FullCalendarModule,
    CommonModule,
    FormsModule,
    NgbModalModule,
    FullCalendarModule,
    ChartModule,
    FileUploadModule,
    NgbPaginationModule
  ],
  providers: [RestService, NavService, DatePipe,AuthGuard ],
  bootstrap: [AppComponent]
})
export class AppModule { }
