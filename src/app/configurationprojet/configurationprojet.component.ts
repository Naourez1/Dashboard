import { Component, OnInit, ViewChild } from '@angular/core';
import { RestService } from '../services/rest.service';
import { MatTableDataSource, MatSort, MatPaginator, MatTable } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { NotificationService } from '../services/notification.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import Swal from "sweetalert2";









@Component({
  selector: 'app-configurationprojet',
  templateUrl: './configurationprojet.component.html',
  styleUrls: ['./configurationprojet.component.scss'],

})
export class ConfigurationprojetComponent implements OnInit {

  date = new FormControl();

  constructor(private titleService: Title, public rest: RestService, public http: HttpClient, public router: Router,private dialog:
     MatDialog, private notificationService: NotificationService ) {
      this.role = localStorage.getItem('role');
      this.id_user = localStorage.getItem('user_id');

console.log("role ===> ",this.role)
      this.list_manger()
     }
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['nomprojet', 'datedebut', 'datefin', 'nomclient', 'nomequipe', 'author',
   'priority', 'modules', 'actions1', 'actions2'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('table') table: MatTable<Element>;
  searchKey: string;
  serializedDate = new FormControl((new Date()).toISOString());
  id_user: any;
  projects: any;
  projets: any;
  projetss: any;
  projetsss: any;
  id_projet: any;
  projetssss: any;
  listedeprojet: any;
  save_state;
  role: any;
  nomequipe: any;
  datee
  nomprojet: any;
  identifiant: any;
  list_mangeres


  priorities = [
    { id: "high", value: 'high' },
    { id: "medium", value: 'medium' },
    { id: "low", value: 'low' }];


    tasks = [
      { id: 1, value: 'Task' }];
    form: FormGroup;

  projet: any = {
    nomprojet:'',
    nomclient:"",
    priority: "",
    datedebut : "",
    datefin: "",
    manager: "",
    estimation: "",
    descreption: "",

  };

  ngOnInit() {
    if(this.role=="admin"){
      console.log("role admin")
      this.getprojet();
    }
      if(this.role=="manager"){
        this.list_pr_by_M()
      }
    if(this.role=="chef_projet"){
      this.list_pr_by_pm()
    }
    if(this.role=="employee"){
      this.list_pr_by_employee()
    }



    this.form = new FormGroup({
      nomprojet: new FormControl('', Validators.required),
      estimation: new FormControl('', Validators.required),
      nomclient: new FormControl('', Validators.required),
      priority: new FormControl(0),
      manager: new FormControl(0),
      datedebut: new FormControl(''),
      datefin: new FormControl(''),
      descreption: new FormControl(''),
    });
  }
  initializeFormGroup() {

  }
  go_toadd(){
    this.save_state='add'
  }
  add_project() {
    console.log( 'before addiing',this.projet)


    this.projet.nomprojet=this.form.get('nomprojet').value
    this.projet.nomclient=this.form.get('nomclient').value
    this.projet.priority=this.form.get('priority').value
    this.projet.datedebut=this.form.get('datedebut').value
    this.projet.datefin=this.form.get('datefin').value
    this.projet.manager=this.form.get('manager').value
    this.projet.estimation=this.form.get('estimation').value
    this.projet.descreption=this.form.get('descreption').value

    this.rest.add_projet(this.projet).subscribe(projet=>{
      this.ngOnInit()
      console.log(projet)


    })

  }
  edit_project() {
    this.projet.nomprojet=this.form.get('nomprojet').value
    this.projet.nomclient=this.form.get('nomclient').value
    this.projet.priority=this.form.get('priority').value
    this.projet.datedebut=this.form.get('datedebut').value
    this.projet.datefin=this.form.get('datefin').value
    this.projet.manager=this.form.get('manager').value
    this.projet.estimation=this.form.get('estimation').value
    this.projet.descreption=this.form.get('descreption').value
    this.rest.edit_projet(this.projet,this.id_projet).subscribe(projet=>{
      this.ngOnInit()
      console.log(projet)


    })

  }
  dodelete(id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You can not go back!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, accept',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.delete_projet(id);

        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Delete successfully'
        }).then(tt => {
          this.ngOnInit();
        })
        /*swalWithBootstrapButtons.fire(
          'Accepté!',
          'Cette annonce a été acceptée.',
          'success'
        ).then(tt => {
          this.ngOnInit();
        })*/
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Canceled',
          '',
          'error'
        )
      }
    })
  }
  go_to_edit(u){
    this.id_projet=u._id
    this.save_state='edit'
   this.form.get('nomprojet').setValue(u.nomprojet)
   this.form.get('nomclient').setValue(u.nomclient)
    this.form.get('priority').setValue(u.priority)
    this.form.get('datedebut').setValue(u.datedebut)
   this.form.get('datefin').setValue(u.datefin)
    this.form.get('manager').setValue(u.manager)
  this.form.get('estimation').setValue(u.estimation)
this.form.get('descreption').setValue(u.descreption)
  }
  delete_projet(id){
    this.rest.delete_p(id).subscribe(p=>{
      this.getprojet()
      console.log(p)
    })
  }
  onClear() {
    this.form.reset();
    this.initializeFormGroup();
    this.notificationService.success(':: Submitted successfully');
  }
 onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  getprojet() {
 this.rest.list_projet().subscribe(result=>{
   console.log(result)
   this.projets=result['all_project']
   console.log("list projet_role_admin",this.projets)
 })
}
list_manger(){
    this.rest.list_managers().subscribe(m=>{
      console.log(m)
      this.list_mangeres=m['accepted_M']
    })
}
edit(r) {
  this.projet = {
    _id: r._id,
    nomprojet: r.nomprojet,
    datedebut: r.datedebut,
    datefin: r.datefin,
    nomclient: r.nomclient,
    nomequipe: r.nomequipe,
    task: r.task,
    priority: r.priority,
    author : r.author
  };


}
test(){
  this.projet.nomprojet=this.form.get('nomprojet').value
  this.projet.nomclient=this.form.get('nomclient').value
  this.projet.priority=this.form.get('priority').value
  this.projet.datedebut=this.form.get('datedebut').value
  this.projet.datefin=this.form.get('datefin').value
  this.projet.manager=this.form.get('manager').value
  this.projet.estimation=this.form.get('estimation').value
  this.projet.description=this.form.get('descreption').value
  console.log(this.projet)
}

  addprojet(u) {
    this.rest.ajouterprojet(u).subscribe(res => {
      console.log('add');
      this.getprojet();
      this.projet = {
        _id: '',
        nomprojet: '',
        datedebut: '',
        datefin: '',
        nomclient: '',
        nomequipe: '',
        task: '',
        priority: '',
        author: ''
      };
      console.log(this.projet);
    });
    this.table.renderRows();
    this.notificationService.success(':: Submitted successfully');

      }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }
  find(u) {
    this.rest.findbyid(u).subscribe(auth => {
      console.log(auth);
      /* tslint:disable:no-string-literal */
      localStorage.setItem('identifiant', auth['resultat']['_id']);
      /* tslint:enable:no-string-literal */
            /* tslint:disable:no-string-literal */
      localStorage.setItem('resultat', JSON.stringify(auth['resultat']));
      /* tslint:enable:no-string-literal */
      this.router.navigate(['home/configlot', u._id]);
    });
  }
  updateprojet(u) {
    this.rest.updateprojet(u).subscribe(res => {
      console.log('done');
      this.getprojet();
      this.projet = {
        _id: '',
        nomprojet: '',
        datedebut: '',
        datefin: '',
        nomclient: '',
        nomequipe: '',
        task: '',
        priority: '',
        author: ''
      };
    });
  }
  remove(id) {

    this.rest.removeprojet(id).subscribe();
    this.getprojet();
    this.notificationService.success(':: Deleted successfully');

  }
  list_pr_by_M(){
    this.rest.list_p_by_M(this.id_user).subscribe(p_m=>{

      this.projets=p_m
      console.log('list project of managers ==> ',this.projets)
    })
  }

  list_pr_by_pm(){
    this.rest.list_p_by_CH(this.id_user).subscribe(p_m=>{

      this.projets=p_m
      console.log('list project of p_manager ==> ',this.projets)
    })
  }
  list_pr_by_employee(){
    this.rest.list_p_by_empl(this.id_user).subscribe(empl=>{
      this.projets=empl
      console.log('list project of p_employee ==> ',this.projets)
    })
  }

}
