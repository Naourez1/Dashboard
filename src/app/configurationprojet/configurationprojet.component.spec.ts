import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigurationprojetComponent } from './configurationprojet.component';

describe('ConfigurationprojetComponent', () => {
  let component: ConfigurationprojetComponent;
  let fixture: ComponentFixture<ConfigurationprojetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigurationprojetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigurationprojetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
