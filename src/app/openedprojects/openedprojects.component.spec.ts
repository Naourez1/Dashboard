import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenedprojectsComponent } from './openedprojects.component';

describe('OpenedprojectsComponent', () => {
  let component: OpenedprojectsComponent;
  let fixture: ComponentFixture<OpenedprojectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenedprojectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenedprojectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
