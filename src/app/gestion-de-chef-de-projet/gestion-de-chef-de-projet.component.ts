import {RestService} from '../services/rest.service';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { NotificationService } from '../services/notification.service';
import { ConfigurationprojetComponent } from '../configurationprojet/configurationprojet.component';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-gestion-de-chef-de-projet',
  templateUrl: './gestion-de-chef-de-projet.component.html',
  styleUrls: ['./gestion-de-chef-de-projet.component.css']
})
export class GestionDeChefDeProjetComponent implements OnInit {
  chefs: any;
  chef: any = {
    _id: '',
    firstname: '',
    lastname: '',
    mail: '',
    status: '',
    position: ''
  };

  constructor(public rest: RestService, public http: HttpClient, public router: Router,
    private dialog: MatDialog, private notificationService: NotificationService  ) {
  this.getchef();

}
  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ['firstname', 'lastname', 'mail', 'status', 'position', 'activity', 'password', 'pic', 'actions'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  searchKey: string;
  getchef() {

    this.rest.getallchef().subscribe(res => this.chefs = res);

  }
  onFileChanged(event) {
    const file = event.target.files[0]
  }
  addchef(u) {

    this.rest.addchef(u).subscribe(res => {
      console.log('add');
      this.getchef();
      this.chef = {
        _id: '',
        firstname: '',
        lastname: '',
        mail: '',
        status: '',
        position: '',
        activity: ''
      };
    })

    ;
  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }


  edit(r) {
    this.chef = {
      _id: r._id,
      firstname: r.firstname,
      lastname: r.lastname,
      mail: r.mail,
      position: r.position,
      status: r.status,
      activity: r.activity
    };


  }

  remove(id) {

    this.rest.removechef(id).subscribe();
    this.getchef();

  }

  updatechef(u) {
    this.rest.updatechef(u).subscribe(res => {
      console.log('done');
      this.getchef();
      this.chef = {
        _id: '',
        firstname: '',
        lastname: '',
        mail: '',
        status: '',
        position: '',
        activity: ''
      };
    });

  }
  ngOnInit() {
    this.listData = new MatTableDataSource(this.chefs);
    this.listData.sort = this.sort;
    this.listData.paginator = this.paginator;
  }
}
