import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionDeChefDeProjetComponent } from './gestion-de-chef-de-projet.component';

describe('GestionDeChefDeProjetComponent', () => {
  let component: GestionDeChefDeProjetComponent;
  let fixture: ComponentFixture<GestionDeChefDeProjetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionDeChefDeProjetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionDeChefDeProjetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
