import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import Swal from "sweetalert2";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
chefs: any;
managers: any;
list_chef;
list_manager
  list_emlpoyee
  p:number=1

  itemsPerPage_m = 3;
  pageSize_m: number;
  currentPage_m = 1;
  itemsPerPage_ch = 3;
  pageSize_ch: number;
  currentPage_ch = 1;
  itemsPerPage_empl = 3;
  pageSize_empl: number;
  currentPage_empl = 1;
  countempl;
  count_chef;
  count_manager

  constructor(public rest: RestService) { }

  ngOnInit() {
    this.list_chefs()
    this.list_managers()
    this.list_employees()
  }
  public onPageChange_m(pageNum: number): void {
    this.pageSize_m = this.itemsPerPage_m*(pageNum - 1);
  }
  public onPageChange_ch(pageNum: number): void {
    this.pageSize_ch = this.itemsPerPage_ch*(pageNum - 1);
  }
  public onPageChange_empl(pageNum: number): void {
    this.pageSize_empl = this.itemsPerPage_empl*(pageNum - 1);
  }
  list_chefs(){
    this.rest.list_chefs().subscribe(chef=>{
      console.log(chef);
      this.count_chef=chef['count']
      this.list_chef=chef['all_chef']
      console.log("listchef",this.list_chef)
      console.log(this.list_chef[0]['image'])

    })
  }
  list_managers(){
    this.rest.list_managers().subscribe(mngrs=>{
      this.count_manager=mngrs['count']
      console.log("mngers",mngrs)
      this.list_manager=mngrs['all_managers']
    })
  }
  list_employees(){
  this.rest.list_employees().subscribe(empl=>{

    this.list_emlpoyee=empl['all_employee']
    this.countempl=empl['count']
  })
  }
  accept(id){
  this.rest.accept_user(id).subscribe(result=>{
    console.log(result)
  })
  }
  refuse(id){
  this.rest.refuse_user(id).subscribe(result=>{
    console.log(result)
  })

  }
  delete(id){
    this.rest.delete(id).subscribe(dlt=>{
      console.log(dlt)
      console.log('done')
    })
  }
  doSwal(indice,id_user) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })
    if(indice == 1) {
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You can not go back!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#673ab7',
        cancelButtonColor: '#b71473',
        confirmButtonText: 'Yes, accept',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
         this.accept(id_user)
          this.ngOnInit();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });

          Toast.fire({
            type: 'success',
            title: 'Accepted successfully'
          }).then(tt => {
            this.ngOnInit();
          })
          /*swalWithBootstrapButtons.fire(
            'Accepté!',
            'Cette annonce a été acceptée.',
            'success'
          ).then(tt => {
            this.ngOnInit();
          })*/
        } else if (
          // Read more about handling dismissals
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Canceled',
            '',
            'error'
          )
        }
      })
    } else {
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You can not go back!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, refuse',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
         this.refuse(id_user)
          this.ngOnInit();
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });

          Toast.fire({
            type: 'success',
            title: 'Successfully rejected'
          }).then(tt => {
            this.ngOnInit();
          })
          /*swalWithBootstrapButtons.fire(
            'Refusée!',
            'Cette annonce a été refusée.',
            'success'
          ).then(tt => {
            this.ngOnInit();
          })*/
        } else if (
          // Read more about handling dismissals
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Canceled',
            '',
            'error'
          )
        }
      })
    }
  }
  dodelete(id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You can not go back!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, accept',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.value) {
          this.delete(id);

          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });

          Toast.fire({
            type: 'success',
            title: 'Delete successfully'
          }).then(tt => {
            this.ngOnInit();
          })
          /*swalWithBootstrapButtons.fire(
            'Accepté!',
            'Cette annonce a été acceptée.',
            'success'
          ).then(tt => {
            this.ngOnInit();
          })*/
        } else if (
          // Read more about handling dismissals
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Canceled',
            '',
            'error'
          )
        }
      })
    }




}
