import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RestService} from '../services/rest.service';
import {NotificationService} from '../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mail: string;
  password: string;

  constructor(public router: Router, public rest: RestService,private notificationService: NotificationService) {
  }

  ngOnInit() {
  }

  login(mail, password) {
    this.rest.login(mail, password).subscribe(auth => {
      console.log(auth);
      if(auth['auth']==true){
        console.log(auth['user']['role'])
        this.router.navigate(["/home/open"])
        localStorage.setItem('logedin',"true")
        localStorage.setItem('user_id',auth['user']['userId'])
        localStorage.setItem('role',auth['user']['role'])
        this.notificationService.success("welcome "+auth['user']['username'])
      }else {
        this.notificationService.warn(auth['message'])
      }
    })

  }
}
