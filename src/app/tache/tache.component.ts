import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {RestService} from '../services/rest.service';

@Component({
  selector: 'app-tache',
  templateUrl: './tache.component.html',
  styleUrls: ['./tache.component.css']
})
export class TacheComponent implements OnInit, OnDestroy {
  idP: number;
  idL: number;
  private sub: any;
  private sub1: any;
  taches: any;
  nomprojet: any;
  nomlot: any;
  role: any;
  tache: any = {
    title: '',
    start: '',
    end: '',
    estimation: '',
    description: '',
    priority: '',

  };

  constructor(public route: ActivatedRoute, public rest: RestService, public http: HttpClient, public router: Router) {
    this.nomprojet = JSON.parse( localStorage.getItem('resultat')).nomprojet,
    this.nomlot = JSON.parse( localStorage.getItem('resultatlot')).nomlot,
    this.role = localStorage.getItem('role');
  }




  edit(r) {
    this.tache = {
      title: r.title,
      start: r.start,
      end: r.end,
      estimation: r.estimation,
      description: r.description,
      priority: r.priority
    };


  }

  findbydate(datedebut , datefin) {
    return datefin - datedebut;
  }

  addtache(id, u) {
    this.rest.ajoutertache1(id, u).subscribe(res => {
      console.log('add');
      this.ngOnInit();
      this.tache = {
        title: '',
        start: '',
        end: '',
        priority : '',
        estimation : '',
        description : ''

      };
    });
  }

  remove(id, idc, idcc) {

    this.rest.removetache(id, idc, idcc).subscribe();
    this.ngOnInit();

  }
  updatetache(id, idc, u) {


    this.rest.updatetache(id, idc, u).subscribe(res => {
      console.log('done');
      this.ngOnInit();


      this.tache = {
        _id: '',
        title: '',
        start: '',
        end: '',
        priority : '',
        estimation : '',
        description : ''
      };
    });

  }


  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      this.idP = params['idP']; // (+) converts string 'id' to a number


      // In a real app: dispatch action to load the details here.
    });
    this.sub1 = this.route.params.subscribe(params => {

      this.idL = params['idL']; // (+) converts string 'id' to a number


      // In a real app: dispatch action to load the details here.
    });
    this.rest.getalltache(this.idP, this.idL).subscribe(res => this.taches = res);

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.sub1.unsubscribe();
  }

}
