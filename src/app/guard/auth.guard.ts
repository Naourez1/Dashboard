import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate  {
  constructor( private router: Router) {
    console.log(localStorage.getItem('logedin'))
  }
  canActivate() {
    console.log('im in')
    if (localStorage.getItem('logedin')=='true'){
      return true
    }else {
      this.router.navigate([""])
    }
  }
}
