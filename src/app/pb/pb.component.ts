import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { Projet } from '../services/projetmodal';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import * as Timeline from '../../assets/js/rtime.js';
import {ActivatedRoute, Router} from '@angular/router';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-pb',
  templateUrl: './pb.component.html',
  styleUrls: ['./pb.component.css']
})
export class PBComponent implements OnInit {
    closeResult: string;
    projets: any;
    taches: any;
    dateproject: any;
    diff: any;
    dateFormat;
    x = new Array();
    id: any;
    now: any;
  constructor(public route: ActivatedRoute, public router: Router, public rest: RestService, private modalService: NgbModal) { }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  diffDays(datedebut , datefin) {
    return (datefin - datedebut) * (-1);
  }
 /*  findtask(id) {
  this.rest.findbyidprojtache(id).subscribe(auth => {
  console.log(auth);
  this.router.navigate(['home/tp']);
});
  } */
  ngOnInit() {
    this.rest.getallprojet().subscribe(res => {
    this.now = new Date();
    this.dateFormat(this.now, "dddd, mmmm dS, yyyy, h:MM:ss TT");
    console.log("its this"+this.now.getDate());
    console.log(res);
    this.projets = res;
    for (let index = 0; index < this.projets.length; index++) {
    const element = this.projets[index].datefin;
    let dateString = element ;
    let newDate = new Date(dateString);
    console.log(newDate.getDate());
    this.diff = newDate.getDate() - this.now.getDate();
    this.x.push(this.diff);
      }
    });

}
}
