import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { NavService} from '../../services/nav.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public route: Router, private navService: NavService) { }

  ngOnInit() {
  }
  logout() {
    this.route.navigate(['']);
    localStorage.clear()

  }
  hideSide() {
    this.navService.hideSideBar = !this.navService.hideSideBar;
}
}
