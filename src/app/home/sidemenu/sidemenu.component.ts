import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, } from '@angular/forms';
import {RestService} from '../../services/rest.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {
  role: any;
  id_user: any;
  firstname: any;
  lastname: any;
  options: FormGroup;
  user
  filesToUploads


  constructor(public rest:RestService) {
    this.role = localStorage.getItem('role');
    this.id_user=localStorage.getItem("user_id")

  }

  ngOnInit() {
    this.one_user()
  }
  one_user(){
    this.rest.one_u(this.id_user).subscribe(result=>{
      console.log('ussseer',result)
      this.user=result
      console.log("imaage",result['image'])
    })
  }
  fileChangeEvent(fileInput:any) {
    this.filesToUploads = <Array<File>>fileInput.target.files;
    console.log(this.filesToUploads[0]);
    const formData: any = new FormData();
    const files:Array<File> = this.filesToUploads;
    formData.append("image",files[0]);

    this.rest.picture(this.id_user,formData).subscribe(result=>{
      console.log(result)
      this.one_user()

    })
  }

}
