import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskfromsprintComponent } from './taskfromsprint.component';

describe('TaskfromsprintComponent', () => {
  let component: TaskfromsprintComponent;
  let fixture: ComponentFixture<TaskfromsprintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskfromsprintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskfromsprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
