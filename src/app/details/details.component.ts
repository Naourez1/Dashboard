import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RestService} from '../services/rest.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from "sweetalert2";
import {NotificationService} from '../services/notification.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  role;
  id_user
  team
  team_id
  assign_empl:boolean=false
  panelOpenState = false;
  id_project
  id_module
  project
  list_modules
  list_chef:any=[]
  list_project_manager:any=[]
  list_empl:any=[]
  arrayempl:any=[]
  save_state;
  module_f: FormGroup;
  module: any = {
    title:'',
    priority: "",
    start_day : "",
    end_day: "",
    estimation: {
      week_nb:'',
      days_nb:'',
      hours_nb:'',
    },
    descreption: "",


  };
  priorities = [
    { id: "high", value: 'high' },
    { id: "medium", value: 'medium' },
    { id: "low", value: 'low' }];
  constructor(public route:ActivatedRoute,public rest:RestService,public notificationService: NotificationService) {
    this.role = localStorage.getItem('role');
    this.id_user = localStorage.getItem('user_id');
    console.log("role ===> ",this.role)
  }
  go_toadd(){
    this.save_state='add'
  }
  go_to_edit(u){
    this.id_module=u._id
    this.save_state='edit'
    this.module_f.get('title').setValue(u.title)
    this.module_f.get('datedebut').setValue(u.start_day)
    this.module_f.get('datefin').setValue(u.end_day)
    this.module_f.get('priority').setValue(u.priority)
    this.module_f.get('descreption').setValue(u.descreption)
    this.module_f.get('week').setValue(u.estimation.week_nb)
    this.module_f.get('day').setValue(u.estimation.days_nb)
    this.module_f.get('hour').setValue(u.estimation.hours_nb)
  }
  dodelete(id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You can not go back!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, accept',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.delete_m(id)


        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Delete successfully'
        }).then(tt => {
          this.ngOnInit();
        })
        /*swalWithBootstrapButtons.fire(
          'Accepté!',
          'Cette annonce a été acceptée.',
          'success'
        ).then(tt => {
          this.ngOnInit();
        })*/
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Canceled',
          '',
          'error'
        )
      }
    })
  }

  ngOnInit() {
    this.module_f = new FormGroup({
      chef: new FormControl('', Validators.required),
      employee: new FormControl('', Validators.required),
      teamname: new FormControl('', Validators.required),
      title: new FormControl('', Validators.required),
      datedebut: new FormControl('', Validators.required),
      datefin: new FormControl('', Validators.required),
      priority: new FormControl('', Validators.required),
      descreption: new FormControl('', Validators.required),
      week: new FormControl('', Validators.required),
      day: new FormControl('', Validators.required),
      hour: new FormControl('', Validators.required),

    });

    this.route.params.subscribe(id=>{
      this.id_project=id['id']
      this.one_p()
    })
   this. list_chefs()
    this.list_employee()
  }
  one_p(){
    this.rest.one_p(this.id_project).subscribe(p=>{
      this.project=p
      this.list_modules=p['Modules']
      this.list_project_manager=p['chef_projet']
      this.team=p['teams']['list_employees']
      this.team_id=p['teams']._id

      console.log("list project manager",this.list_project_manager)
      console.log("list employee",this.team)

      console.log(p)
    })
  }
  add_module() {
    this.module.title=this.module_f.get('title').value
    this.module.start_day=this.module_f.get('datedebut').value
    this.module.end_day=this.module_f.get('datefin').value
    this.module.priority=this.module_f.get('priority').value
    this.module.descreption=this.module_f.get('descreption').value
    this.module.estimation.week_nb=this.module_f.get('week').value
    this.module.estimation.days_nb=this.module_f.get('day').value
    this.module.estimation.hours_nb=this.module_f.get('hour').value
    console.log( 'before addiing',this.module)
    this.rest.add_module(this.module,this.id_project).subscribe(m=>{
      this.one_p()
      console.log(m)
    })
  }
  edit_module(){
    this.module.title=this.module_f.get('title').value
    this.module.start_day=this.module_f.get('datedebut').value
    this.module.end_day=this.module_f.get('datefin').value
    this.module.priority=this.module_f.get('priority').value
    this.module.descreption=this.module_f.get('descreption').value
    this.module.estimation.week_nb=this.module_f.get('week').value
    this.module.estimation.days_nb=this.module_f.get('day').value
    this.module.estimation.hours_nb=this.module_f.get('hour').value
    console.log( 'before addiing',this.module)
    this.rest.edit_m(this.id_module,this.module).subscribe(m=>{
      this.one_p()
      console.log(m)
    })

  }
  delete_m(id){
    this.rest.delete_m(id,this.id_project).subscribe(m=>{
      console.log(m)
      this.one_p()
    })
  }
  list_chefs(){
    this.rest.list_chefs().subscribe(ch=>{
      console.log('list chef =>',ch)
      this.list_chef=ch['accepted_ch']

    })
  }
  list_employee(){
    this.rest.list_employees().subscribe(empl=>{

      this.list_empl=empl['accepted_empl']
      console.log("list_empl ==> ",this.list_empl)
    })
  }
assign_project_manager(){
    let id_chef=this.module_f.get('chef').value
    this.rest.affect_chef(this.id_project,id_chef).subscribe(result=>{
      console.log(result)
      if (result['state']=="done") {
        this.notificationService.success(result['message'])
        this.one_p()
      }else{
        this.notificationService.warn(result['message'])
      }
    })

}
test(){
    console.log(this.module_f.get('employee').value)
}
addteam(){
    let team_name=this.module_f.get('teamname').value
  this.arrayempl=this.module_f.get('employee').value
   let team={
     team_name:team_name,
     list_employees:this.arrayempl

   }
   console.log(team)
  this.rest.add_team(team).subscribe(team=>{
    let id_team=team['result']._id
    this.rest.affect_team(this.id_project,id_team).subscribe(te=>{
      console.log('im in affection')
      console.log('affectation team',te)
      this.one_p()

    })
  })
  }
  assign_employee(){
    this.assign_empl=true
  }
  cancel_assign(){
    this.assign_empl=false
  }
  add_employee(){
    this.rest.add_employee(this.team_id,this.module_f.get('employee').value).subscribe(result=>{
      console.log("add emplouyee",result)
      this.assign_empl=false
      this.one_p()
    })
  }


}
