import { Component, OnInit } from '@angular/core';
import {RestService} from '../../services/rest.service';
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import Swal from "sweetalert2";

@Component({
  selector: 'app-details-m',
  templateUrl: './details-m.component.html',
  styleUrls: ['./details-m.component.css']
})
export class DetailsMComponent implements OnInit {
  role;
  id_user
  id_module;
  id_sprint
  module;
  list_sprint
  save_state;
  sprint_f: FormGroup;
  sprint: any = {
    title:'',
    priority: "",
    start_day : "",
    end_day: "",

    estimation: {
      week_nb:'',
      days_nb:'',
      hours_nb:'',
    },
    descreption: "",
  };
  priorities = [
    { id: "high", value: 'high' },
    { id: "medium", value: 'medium' },
    { id: "low", value: 'low' }];
  constructor(public rest:RestService,public route:ActivatedRoute) {
    this.role = localStorage.getItem('role');
    this.id_user = localStorage.getItem('user_id');
    console.log("role ===> ",this.role)
  }
  go_toadd(){
    this.save_state='add'
  }
  go_to_edit(u){
    this.id_sprint=u._id
    this.save_state='edit'
    this.sprint_f.get('title').setValue(u.title)
    this.sprint_f.get('start_day').setValue(u.start_day)
    this.sprint_f.get('end_day').setValue(u.end_day)
    this.sprint_f.get('priority').setValue(u.priority)
    this.sprint_f.get('descreption').setValue(u.descreption)
    this.sprint_f.get('week').setValue(u.estimation.week_nb)
    this.sprint_f.get('day').setValue(u.estimation.days_nb)
    this.sprint_f.get('hour').setValue(u.estimation.hours_nb)
  }
  dodelete(id) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You can not go back!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, accept',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.delete_s(id)


        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Delete successfully'
        }).then(tt => {
          this.ngOnInit();
        })
        /*swalWithBootstrapButtons.fire(
          'Accepté!',
          'Cette annonce a été acceptée.',
          'success'
        ).then(tt => {
          this.ngOnInit();
        })*/
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Canceled',
          '',
          'error'
        )
      }
    })
  }
  
  ngOnInit() {
    this.sprint_f = new FormGroup({
      title: new FormControl('', Validators.required),
      start_day: new FormControl('', Validators.required),
      end_day: new FormControl('', Validators.required),
      priority: new FormControl('', Validators.required),
      descreption: new FormControl('', Validators.required),
      week: new FormControl('', Validators.required),
      day: new FormControl('', Validators.required),
      hour: new FormControl('', Validators.required),

    });
    this.route.params.subscribe(id=>{
      this.id_module=id['id']
      this.one_m()
    })
  }
  one_m(){
    this.rest.one_m(this.id_module).subscribe(m=>{

    this.module=m
      this.list_sprint=m['Sprints']
    console.log(m)
  })
  }
  add_sprint() {
    this.sprint.title=this.sprint_f.get('title').value
    this.sprint.start_day=this.sprint_f.get('start_day').value
    this.sprint.end_day=this.sprint_f.get('end_day').value
    this.sprint.priority=this.sprint_f.get('priority').value
    this.sprint.descreption=this.sprint_f.get('descreption').value
    this.sprint.estimation.week_nb=this.sprint_f.get('week').value
    this.sprint.estimation.days_nb=this.sprint_f.get('day').value
    this.sprint.estimation.hours_nb=this.sprint_f.get('hour').value
    console.log( 'before addiing',this.sprint)
    this.rest.add_sprint(this.sprint,this.id_module).subscribe(s=>{
      this.one_m()
      console.log(s)
    })

  }
  edit_sprint(){
    this.sprint.title=this.sprint_f.get('title').value
    this.sprint.start_day=this.sprint_f.get('start_day').value
    this.sprint.end_day=this.sprint_f.get('end_day').value
    this.sprint.priority=this.sprint_f.get('priority').value
    this.sprint.descreption=this.sprint_f.get('descreption').value
    this.sprint.estimation.week_nb=this.sprint_f.get('week').value
    this.sprint.estimation.days_nb=this.sprint_f.get('day').value
    this.sprint.estimation.hours_nb=this.sprint_f.get('hour').value
    console.log( 'before editing',this.sprint)
    console.log('idsprint',this.id_sprint)
    this.rest.edit_s(this.id_sprint,this.sprint).subscribe(s=>{
      console.log('im in edit')
      console.log(s)
      this.one_m()
    })
  }
  delete_s(id){
    this.rest.delete_s(id,this.id_module).subscribe(m=>{
      console.log(m)
      this.one_m()
    })
  }

}
