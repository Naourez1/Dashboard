import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsMComponent } from './details-m.component';

describe('DetailsMComponent', () => {
  let component: DetailsMComponent;
  let fixture: ComponentFixture<DetailsMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
