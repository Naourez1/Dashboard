import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RestService} from '../../../services/rest.service';
import {ActivatedRoute} from '@angular/router';
import Swal from "sweetalert2";

@Component({
  selector: 'app-details-s',
  templateUrl: './details-s.component.html',
  styleUrls: ['./details-s.component.css']
})
export class DetailsSComponent implements OnInit {
  role;
  id_user
  id_tache
  id_sprint
  list_taches
  sprint
  save_state;
  tache_f: FormGroup;
  tache: any = {
    title:'',
    priority: "",
    start_day : "",
    end_day: "",
    estimation: {
      week_nb:'',
      days_nb:'',
      hours_nb:'',
    },
  };
  priorities = [
    { id: "high", value: 'high' },
    { id: "medium", value: 'medium' },
    { id: "low", value: 'low' }];
  constructor(public rest:RestService,public route:ActivatedRoute) {
    this.role = localStorage.getItem('role');
    this.id_user = localStorage.getItem('user_id');
    console.log("role ===> ",this.role)
  }
  go_toadd(){
    this.save_state='add'
  }
  go_to_edit(u){
    this.id_tache=u._id
    this.save_state='edit'
    this.tache_f.get('title').setValue(u.title)
    this.tache_f.get('start_day').setValue(u.start_day)
    this.tache_f.get('end_day').setValue(u.end_day)
    this.tache_f.get('priority').setValue(u.priority)
    this.tache_f.get('week').setValue(u.estimation.week_nb)
    this.tache_f.get('day').setValue(u.estimation.days_nb)
    this.tache_f.get('hour').setValue(u.estimation.hours_nb)
  }
  dodelete(id) {
    console.log('id tache pour supprimer',id)
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: 'Are you sure?',
      text: "You can not go back!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, accept',
      cancelButtonText: 'No, cancel!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
       this.delete_t(id)


        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });

        Toast.fire({
          type: 'success',
          title: 'Delete successfully'
        }).then(tt => {
          this.ngOnInit();
        })
        /*swalWithBootstrapButtons.fire(
          'Accepté!',
          'Cette annonce a été acceptée.',
          'success'
        ).then(tt => {
          this.ngOnInit();
        })*/
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Canceled',
          '',
          'error'
        )
      }
    })
  }
  ngOnInit() {
    this.tache_f = new FormGroup({
      title: new FormControl('', Validators.required),
      start_day: new FormControl('', Validators.required),
      end_day: new FormControl('', Validators.required),
      priority: new FormControl('', Validators.required),
      week: new FormControl('', Validators.required),
      day: new FormControl('', Validators.required),
      hour: new FormControl('', Validators.required),

    });
    this.route.params.subscribe(id=>{
      this.id_sprint=id['id']
      this.one_s()
    })
  }
  one_s(){
    this.rest.one_s(this.id_sprint).subscribe(s=>{
      this.sprint=s
      console.log('first table sprint details',this.sprint)
      this.list_taches=s['taches']
    })
  }
  add_tache() {
    this.tache.title=this.tache_f.get('title').value
    this.tache.start_day=this.tache_f.get('start_day').value
    this.tache.end_day=this.tache_f.get('end_day').value
    this.tache.priority=this.tache_f.get('priority').value
    this.tache.estimation.week_nb=this.tache_f.get('week').value
    this.tache.estimation.days_nb=this.tache_f.get('day').value
    this.tache.estimation.hours_nb=this.tache_f.get('hour').value
    this.rest.add_tache(this.tache,this.id_sprint).subscribe(t=>{
      console.log('new tache',t)
      this.one_s()
    })

  }
  edit_tache(){
    this.tache.title=this.tache_f.get('title').value
    this.tache.start_day=this.tache_f.get('start_day').value
    this.tache.end_day=this.tache_f.get('end_day').value
    this.tache.priority=this.tache_f.get('priority').value
    this.tache.estimation.week_nb=this.tache_f.get('week').value
    this.tache.estimation.days_nb=this.tache_f.get('day').value
    this.tache.estimation.hours_nb=this.tache_f.get('hour').value
    console.log( 'before editing',this.sprint)
    console.log('idsprint',this.id_sprint)
    this.rest.edit_t(this.id_tache,this.tache).subscribe(s=>{
      console.log('im in edit')
      console.log(s)
      this.one_s()
    })
  }
  delete_t(id){
    this.rest.delete_t(id,this.id_sprint).subscribe(m=>{
      this.one_s()
    })
  }

}
