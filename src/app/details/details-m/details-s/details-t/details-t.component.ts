import { Component, OnInit } from '@angular/core';
import {RestService} from '../../../../services/rest.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-details-t',
  templateUrl: './details-t.component.html',
  styleUrls: ['./details-t.component.css']
})
export class DetailsTComponent implements OnInit {
id_tache
  tache
  role;
  id_user
  constructor(public rest:RestService,public route:ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(id=>{
      this.id_tache=id['id']
      this.one_t()
    })
  }
  one_t(){
    this.rest.one_t(this.id_tache).subscribe(t=>{
      console.log(t)
      this.tache=t
    })
  }

}
