import { Component, OnInit, OnDestroy } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { RestService } from '../services/rest.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { PBComponent } from '../pb/pb.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-taskfromproj',
  templateUrl: './taskfromproj.component.html',
  styleUrls: ['./taskfromproj.component.css']
})
export class TaskfromprojComponent implements OnInit, OnDestroy {
  id: number;
  private sub: any;
  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];
  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];
 tasks: any;
  constructor(public rest: RestService, private route: ActivatedRoute) {
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }
/*   getalltache(){
    this.rest.getalltache(this.id, this.idc).subscribe(res => {

    this.tasks = res
    console.log(this.tasks)
  }
      );
  } */
  
  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      this.id = params['id']; // (+) converts string 'id' to a number
      // In a real app: dispatch action to load the details here.
    });
    this.getTasksByProject();
    }
    getTasksByProject() {
      this.rest.findbyidprojtache(this.id).subscribe(auth => {
        this.tasks = auth;
        console.log(this.tasks);
      });
    }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
