import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user={
    firstname:"",
    Lastname:"",
    Mail:"",
    Username:"",
    password:"",
    role:""
  }
  constructor(public rest: RestService, public router: Router) { }

  ngOnInit() {
  }
  // tslint:disable-next-line:whitespace
  registre() {
this.rest.registre(this.user).subscribe(res=>{
  this.router.navigate([''])


})


  }
 
}
