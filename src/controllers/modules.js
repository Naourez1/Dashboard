const modulesModel=require('../models/modules')
const projetModel=require('../models/projetmodel')
var mongoose = require("mongoose");

module.exports = {
  add_modules:function (req,res) {
    let idprojet=req.params.idp
    modulesModel.create(req.body)

      .then(modules=>{
        projetModel.findById(idprojet)
          .then(projet=>{
            projet["Modules"].push(modules['_id'])
            projet.save()
              .then(save=>{
                res.json(modules)
              })
              .catch(errsave=>{

                return errsave
              })
          })
          .catch(errprojet=>{
            return errprojet
          })

      })
      .catch(error=>{
        res.json(error)
      })
  },
  update_module:function (req,res) {
    let idmodule=req.params.id
    modulesModel.findByIdAndUpdate(idmodule,req.body)
      .then(result=>{
        res.json(result)
      })
      .catch(error=>{
        res.json(error)
      })
  },
  module_by_id:function (req,res) {
    let idmodule=req.params.id
    modulesModel.findById(idmodule)
      .populate('Sprints')
      .then(result=>{
        res.json(result)
      })
      .catch(error=>{
        res.json(error)
      })
  },
  delete_m:function (req,res) {
    let idmodule=req.params.id
    let idproject=req.params.idp
    projetModel.findOneAndUpdate(idproject,{$pull:{Modules:{$in:idmodule}}}).then(p=>{
      modulesModel.findByIdAndDelete(idmodule).then(del=>{
        res.json('deleted')
      })
        .catch(err=>{
          res.json(err)
        })

    })
      .catch(error=>{
        return res.json(error)
      })


  }


}
