
const projetModel=require('../models/projetmodel')
const teamModel=require('../models/Team')
var mongoose = require("mongoose");

module.exports = {
  projet_admin: function (req, res) {
    projetModel.create(req.body)
      .then(result=>{
        console.log('body ',result)
        res.send({message:"add with success",result:result})
      })
      .catch(err=>{
      res.json({error:err})
      })
  },
  update_admin: function (req, res) {
    let project_id=req.params.id
    projetModel.findByIdAndUpdate(project_id,req.body)

      .then(result=>{

        res.send({message:"update with success",result:result})
      })
      .catch(err=>{
        res.json({error:err})
      })
  },
  delete_project:function (req,res) {
    let project_id=req.params.id
    projetModel.findByIdAndDelete(project_id)
      .then(supp=>{
        res.json('supp with success')
      })
      .catch(err=>{
        res.send(err)
      })
    
  },
  list_project:function (req,res) {
    projetModel.find()
      .populate('manager chef_projet teams')
      .then(project=>{
        const rep={

          count:project.length,
          all_project:project,
          haut_p:project.filter(function (m) {
            return m.priority==='haut'

          }),
          moyen_p:project.filter(function (m) {
            return m.priority==='moyen'

          }),
          bas_p:project.filter(function (m) {
            return m.priority==='bas'
          })
        }
        res.json(rep)

      })

  },
  one_p:function (req,res) {
    let projectid=req.params.id

    projetModel.findById(projectid)
      .populate('manager chef_projet  Modules ')
      .populate({
        path:"teams",
        populate:{
          path:"list_employees"
        }
      })
      .
      then(project=>{
        res.json(project)
      })
      .
      catch(err=>{
        res.json(err)
      })

  },
  affectation_manager:function (req,res) {
    let projectid=req.params.idp
    let managerid=req.params.idm
    projetModel.findById(projectid)
      .then(project=>{
        project['manager'].push(managerid)
        project.save()
          .then(result=>{
            res.json(result)
          })
          .catch(err=>{
            res.json(err)
          })
      })
      .catch(err=>{
        return err
      })
    
  },
  affectation_chef:function (req,res) {
    let projectid=req.params.idp
    let chef_id=req.params.idch
    projetModel.findById(projectid)
      .then(project=>{
        project['chef_projet'].push(chef_id)
        project.save()
          .then(result=>{
            res.json({result:result,message:"Project manager successfully assigned ",state:"done"})
          })
          .catch(err=>{
            res.json({err:err,message:"Something went wrong",state:'not done'})
          })
      })
      .catch(err=>{
        return err
      })

  },
  list_project_by_manager:function (req,res) {
    let managerid=req.params.id
    projetModel.find( { 'manager': mongoose.Types.ObjectId(managerid) } )
      .select('-manager')
      .then(result=>{
        res.json(result)

      })
      .catch(err=>{
        res.json(err)
      })



  },
  list_project_by_chef:function (req,res) {
    let chef_id=req.params.id
    projetModel.find( { 'chef_projet': mongoose.Types.ObjectId(chef_id) } )
      .select('-chef_projet')
      .then(result=>{
        res.json(result)
      })
      .catch(err=>{
        res.json(err)
      })
  },
  list_project_by_employee:function(req,res){
    let id_empl=req.params.idempl
    teamModel.find({'list_employees': mongoose.Types.ObjectId(id_empl)})
      .then(result=>{
        let idteam=result[0]['_id']
        projetModel.find({'teams': mongoose.Types.ObjectId(idteam)})
          .then(result=>{
            res.json(result)
          })
          .catch(err=>{
            res.json(err)
          })
      })
      .catch(error=>{
return res.json(error)
      })

  },
  affectation_team:function (req,res) {
    console.log('im in affect team')
    let projectid=req.params.idp
    let team_id=req.params.idt
    projetModel.findByIdAndUpdate(projectid,{$set:{teams:team_id}})
      .then(project=>{
        res.json(project)

      })
      .catch(err=>{
        res.json(err)
      })

  },

}


