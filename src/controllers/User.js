const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const userModel=require('../models/user.M')
const tokenList = {};


module.exports = {

  creat: function (req, res) {
    console.log('body ',req.body)
    userModel.create(req.body)
      .then(result=>{
        console.log('body ',result)
        res.send({message:"add with success",result:result})
      })
      .catch(err=>{
       if (err.code===11000){
         res.json({message:"Email is already in use"})
       }else{
         res.json({error:err})
       }
      })
  },
  addphoto:function(req,res){
    console.log(req.file)
  let image=req.file.path
    let userid=req.params.id

  userModel.findByIdAndUpdate(userid,{$set:{image:image}})
    .then(pic=>{
      res.json({message:'picture add with success'})
    })
    .catch(err=>{
      res.json(err)
    })
  },
  update: function (req, res) {
    let userid=req.params.id
    console.log('update ',req.body)
    userModel.findByIdAndUpdate(userid,req.body)
      .then(result=>{
        res.send({message:"updated with success",result:result})
      })
      .catch(err=>{
        if (err.code===11000){
          res.json({message:"Email is already in use"})
        }else{
          res.json({error:err})
        }
      })
  },
  userbyid:function (req,res) {
    let userid=req.params.id
    userModel.findById(userid)
      .then(user=>{
        res.json(user)
      })
      .catch(err=>{
        res.json(err)
      })

  },

  deleteuser:function (req,res) {
    let userid=req.params.id
    userModel.findByIdAndDelete(userid)
      .then(user=>{
        res.json({message:'deleted with success'})
      })
      .catch(err=>{
        res.json(err)
      })

  },
  list_manager:function (req,res) {
    userModel.find({role:"manager"})
      .then(managers=>{
        const rep={

          count:managers.length,
          all_managers:managers,
          pending_M:managers.filter(function (m) {
            return m.etat==='0'

          }),
          accepted_M:managers.filter(function (m) {
            return m.etat==='1'

          }),
          refused_M:managers.filter(function (m) {
            return m.etat==='2'
          })
        }
        res.json(rep)

      })
      .catch(err=>{
        res.json(err)
      })

  },
  list_chef_de_projet:function (req,res) {
    userModel.find({role:"chef_projet"})
      .then(chef=>{
        const rep={


          count:chef.length,
          all_chef:chef,
          pending_ch:chef.filter(function (c) {
            return c.etat==='0'

          }),
          accepted_ch:chef.filter(function (c) {
            return c.etat==='1'

          }),
          refused_ch:chef.filter(function (c) {
            return c.etat==='2'
          })
        }
        res.json(rep)

      })
      .catch(err=>{
        res.json(err)
      })

  },
  list_employer:function (req,res) {
    userModel.find({role:"employee"})
      .then(empl=>{
        const rep={

          count:empl.length,
          all_employee:empl,
          pending_empl:empl.filter(function (e) {
            return e.etat==='0'

          }),
          accepted_empl:empl.filter(function (e) {
            return e.etat==='1'

          }),
          refused_empl:empl.filter(function (e) {
            return e.etat==='2'
          })
        }
        res.json(rep)

      })
      .catch(err=>{
        res.json(err)
      })
  },
  accept_user:function (req,res) {
    let userid=req.params.id
    userModel.findByIdAndUpdate(userid,{$set:{etat:'1'}})

      .then(acc=>{
        res.json({message:"accepted"})
      })

  },
  refuse_user:function (req,res) {
    let userid=req.params.id
    userModel.findByIdAndUpdate(userid,{$set:{etat:'2'}})

      .then(acc=>{
        res.json({message:"refused"})
      })

  },
  authenticate: function (req, res) {
    userModel.findOne({Mail: req.body.mail}, function (err, userInfo) {
      console.log('useeeeeeeeeeeer',userInfo)
      if(err) return res.json({message:"check your server",auth:false});
      if(!userInfo) return res.json({message:"No users found, Check your email",auth:false})
      if(userInfo['etat']==="0") return  res.json({message:"this account is not accepted yet",auth:false})
      if(userInfo['etat']==="2") return  res.json({message:"this account is refused",auth:false})
      const compar=bcrypt.compareSync(req.body.password,userInfo.password);
      if(!compar) return res.json({message:"incorrect password please try again",auth:false})
      const token = jwt.sign({user_id:userInfo._id},'secret', { expiresIn: '2h' });
      const refreshToken = jwt.sign(
        {
          user_id:userInfo._id
        },
        'refresh-secret',
        {
          expiresIn: "30h"
        }
      );
      const response = {
        message: "Logged in",
        token: token,
        refreshToken: refreshToken,
        user: {
          mail: userInfo.Mail,
          role: userInfo.role,
          userId: userInfo._id,
          username:userInfo.Username
        },
        auth:true
      };
      tokenList[refreshToken] = response;


      res.status(200).json(response);
    });
  },
}


