const modulesModel=require('../models/modules')
const sprintModel=require('../models/Sprints')
var mongoose = require("mongoose");

module.exports = {
  add_sprint:function (req,res) {
    let idmodule=req.params.idm
    sprintModel.create(req.body)
      .then(sprint=>{
        modulesModel.findById(idmodule)
          .then(module=>{
            module['Sprints'].push(sprint._id)
              module.save()
                .then(save=>{
                  res.json(sprint)
                })
                .catch(errsave=>{
                  res.json(errsave)
                })
          })
          .catch(errmodule=>{
            return res.json(errmodule)
          })

      })
      .catch(sprinterr=>{
        return res.json(sprinterr)
      })

  },
  update_module:function (req,res) {
    let idsprint=req.params.id
    sprintModel.findByIdAndUpdate(idsprint,req.body)
      .then(result=>{
        console.log("done edit")
        res.json(result)
      })
      .catch(error=>{
        res.json(error)
      })
  },
  one_s:function (req,res) {
    let id_s=req.params.id
    sprintModel.findById(id_s)
      .populate('taches')
      .then(s=>{
      res.json(s)
    })
      .catch(err=>{
        res.json(err)
      })

  },
  delete_s:function (req,res) {
     let id_s=req.params.id
    let id_m=req.params.idm
    modulesModel.findOneAndUpdate(id_m,{$pull:{Sprints:{$in:id_s}}})
      .then(m=>{
        sprintModel.findByIdAndDelete(id_s)
          .then(s=>{
            res.json('deleted')
          })
          .catch(err=>{
            res.json(err)
          })
      })
      .catch(error=>{
        return res.json(error)
      })
  }
}

