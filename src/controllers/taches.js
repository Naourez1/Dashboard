const tachesModel=require('../models/taches')
const sprint=require('../models/Sprints')
var mongoose = require("mongoose");

module.exports = {
  add_taches: function (req, res) {
    let idsprint=req.params.ids
    tachesModel.create(req.body)
      .then(taches => {
        sprint.findById(idsprint)
          .then(sprint=>{
            sprint['taches'].push(taches['_id'])
            sprint.save()
              .then(resultf=>{
                res.send(resultf)
              })
              .catch(errsave=>{
                return errsave
              })
          })
          .catch(errmodule=>{
            return errmodule
          })


      })
      .catch(error => {
        return error
      })
  },
  update_tache: function (req, res) {
    let idtaches = req.params.id
    console.log('bodyyyyyyyyyy',req.body)
    tachesModel.findByIdAndUpdate(idtaches,req.body)

      .then(result => {
        console.log('im in update')
        res.json(result)
      })
      .catch(error => {
        res.json(error)
      })
  },
  delete_tache:function (req,res) {
    let idtaches = req.params.id
    let idsprint = req.params.ids
    sprint.findOneAndUpdate(idsprint,{$pull:{taches:{$in:idtaches}}})
      .then(s=>{
        console.log(s)
        tachesModel.findByIdAndDelete(idtaches)
          .then(t=>{
            res.json('deleted')
          })
          .catch(err=>{
            res.json(err)
          })
      })
      .catch(error=>{
        return res.json(error)
      })

  },
 one_tache:function (req,res) {
    let idtaches = req.params.id
   tachesModel.findById(idtaches)
          .then(t=>{
            res.json(t)
          })
          .catch(err=>{
            res.json(err)
          })
 },


}
