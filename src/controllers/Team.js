



const teamModel=require('../models/Team')
var mongoose = require("mongoose");


module.exports = {
add_team: function (req, res) {
  teamModel.create(req.body)
    .then(result=>{
      res.send({message:"add with success",result:result})
    })
    .catch(err=>{
      res.json({error:err})
    })
},
 update_team: function (req, res) {
   let idteam=req.params.idt
    teamModel.findByIdAndUpdate(idteam,req.body)
      .then(result=>{
        res.send({message:"update with success",result:result})
      })
      .catch(err=>{
        res.json({error:err})
      })
  },
  supp_team: function (req, res) {
  let idteam=req.params.idt
let idemployee=req.params.idempl
    teamModel.findOneAndUpdate(idteam,{$pull:{list_employees:{$in:idemployee}}})
      .then(result=>{
        res.send({message:"supp with success",result:result})
      })
      .catch(err=>{
        res.json({error:err})
      })
  },
  one_team:function (req,res) {
  let idteam=req.params.id
    teamModel.findById(idteam)
      .populate("list_employees")
      .then(result=>{
        res.json(result)

      })
      .catch(err=>{
        console.log(err)
      })

  },
  all_team:function (req,res) {
    let idteam=req.params.id
    teamModel.find()
      .populate("list_employees")
      .then(result=>{
        res.json(result)
      })
      .catch(err=>{
        res.json(err)
      })

  },
  add_employee:function(req,res){
  let idteam=req.params.idt
    let idempl=req.params.idempl
    teamModel.findById(idteam).then(team=>{
      team['list_employees'].push(idempl)
      team.save()
        .then(result=>{
          res.json(result)
        })
        .catch(errr=>{
          res.json(errr)
        })
    })
      .catch(error=>{
        return res.json(error)
      })
  }

}
