const express = require('express');
const router = express();
const projetController = require('../controllers/PROJET');
router.get('/list_project',projetController.list_project)

router.get('/affect_m/:idp/:idm',projetController.affectation_manager)

router.get('/affect_ch/:idp/:idch',projetController.affectation_chef)
router.get('/affectation_team/:idp/:idt',projetController.affectation_team)

router.post('/add_project',projetController.projet_admin)
router.get('/one_p/:id',projetController.one_p)

router.put('/update_project/:id',projetController.update_admin)

router.delete('/delete_project/:id',projetController.delete_project)

router.get('/list_p_by_m/:id',projetController.list_project_by_manager)

router.get('/list_p_by_chef/:id',projetController.list_project_by_chef)
router.get('/list_p_by_employee/:idempl',projetController.list_project_by_employee)

module.exports = router;
