var express = require("express");
var db = require("../models/db");
var projetmodel = require("../models/projetmodel");
var lotmodel = require("../models/lotmodel");
var tachemodel = require("../models/tachemodel");
var router = express.Router();

router.get("/", function (req, res) {
  res.send("Hello Admin")

});
  router.get("/findbydate", function (req, res) {
    projetmodel.find({dateA: req.query.datedebut , datef:req.query.datefin}, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        res.send(result)
      }
  
    })
  
  });
 /*  router.get("/authors" , function(req,res) {
   var author ;
   projetmodel.find({}, function (err, result) {
    res.forEach(e => {
      author.add()
    }).
    res.send(author)

  })
  }); */
  router.get("/filterProject" , function (req, res) {
    projetmodel.find({author : req.query.author } , function(err , result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        res.send(result)
      }
    })
  })
  
  router.get("/add", function (req, res) {
    var adm = new projetmodel({
      nomprojet: req.query.nomprojet,
      datedebut: req.query.datedebut,
      datefin: req.query.datefin,
      nomclient: req.query.nomclient,
      nomequipe: req.query.nomequipe,
      author: req.query.author,
      task: req.query.task,
      priority: req.query.priority,
      listedelot: []
  
    });
    adm.save(function (err) {
      if (err) {
        res.send({err: "there are an error"});
        throw err
      }
      res.send({add: true})
  
    })
  
  });
  router.get("/update", function (req, res) {
    projetmodel.findByIdAndUpdate(req.query.id, {
      nomprojet: req.query.nomprojet,
      datedebut: req.query.datedebut,
      datefin: req.query.datefin,
      nomclient: req.query.nomclient,
      nomequipe: req.query.nomequipe,
      author: req.query.author,
      task: req.query.task,
      priority: req.query.priority
        }, {new: true}, function (err) {
      if (err) {
        res.send({err: "there are an error"});
        throw err
      }
      res.send({update: true})
  
    })
  
  });
  router.get("/findbyid", function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"});
        throw err
      }
      res.send({resultat: result})
    })
  });
 
  router.get("/:id/gettaskbyidproj",(req,res,next)=>{
    var projId=req.params.id;
    tachemodel.find({project_id:projId}).populate('projetmodel').exec(function(err,result){
        if (err)
        {
            res.send(err);
        }
        if (!result)
        {
            res.status(404).send();
        }
        else
        {
            res.send(result);
        }
    });
  })

  router.get("/:id/getlotbyidproj",(req,res,next)=>{
    var projId=req.params.id;
    lotmodel.find({project_id:projId}).populate('projetmodel').exec(function(err,result){
        if (err)
        {
            res.send(err);
        }
        if (!result)
        {
            res.status(404).send();
        }
        else
        {
            res.send(result);
        }
    });
  })

  router.get("/findbyidlot", function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        for (var i = 0; i < result.listedelot.length; i++) {
          if (result.listedelot[i]._id == req.query.idc) {
            res.send({resultatlot: result.listedelot[i]})
          }
  
        }
      }
  
    })
  
  })
  router.get("/all", function (req, res) {
    projetmodel.find({}, function (err, result) {
      res.send(result)
  
    })
  
  });
  router.get("/findbyname", function (req, res) {
    projetmodel.find({nomequipe: req.query.nomequipe}, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        res.send(result)
      }
  
    })
  //manager ynajm ychouf listes des projets
  })

  router.get("/findbynameprojet", function (req, res) {
    chefdeprojetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        var tab = []
        this.tab = []
        var tab1 = []
        this.tab1 = []
        for (var i = 0; i < result.listedesprojets.length; i++) {
          this.tab.push(result.listedesprojets[i].nomprojet)
        }
        projetmodel.find({nomequipe: req.query.nomequipe}, function (err, result) {
          if (err) {
            res.send({err: "there are an error"})
            throw err
          } else {
  
            for (var i = 0; i < result.length; i++) {
              for (var j = 0; j < this.tab.length; j++) {
                if (this.tab[j] == result[i].nomprojet) {
                  this.tab1.push(result[i])
  
                }
  
              }
            }
            res.send(this.tab1)
  
  
          }
  
        })
      }
  
    })
  
  })
  router.get("/findbynameprojetemp", function (req, res) {
    employemodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        var tab = []
        this.tab = []
        var tab1 = []
        this.tab1 = []
        for (var i = 0; i < result.listedesprojets.length; i++) {
          this.tab.push(result.listedesprojets[i].nomprojet)
        }
        projetmodel.find({nomequipe: req.query.nomequipe}, function (err, result) {
          if (err) {
            res.send({err: "there are an error"})
            throw err
          } else {
  
            for (var i = 0; i < result.length; i++) {
              for (var j = 0; j < this.tab.length; j++) {
                if (this.tab[j] == result[i].nomprojet) {
                  this.tab1.push(result[i])
  
                }
  
              }
            }
            res.send(this.tab1)
  
  
          }
  
        })
      }
  
    })
  
  })
  router.get("/remove", function (req, res) {
    projetmodel.remove({_id: req.query.id}, function (err) {
      if (err) {
        res.send({err: "there are an error"});
        throw err
      }
      res.send({remove: true})
    })
  
  });
  router.get("/ajouterlot", function (req, res) {
    projetmodel.findById(req.query.id, function (err, admin) {
      if (err) {
        res.send({err: "there are an error"});
        throw err
      } else {
        var ta = new lotmodel({
          nomlot: req.query.nomlot,
          datedebut: req.query.datedebut,
          datefin: req.query.datefin,
          listedetache: []
  
        });
        admin.listedelot.push(ta);
        admin.save(function (err) {
          if (err) {
            res.send({err: 'there are an error'})
          } else {
            res.send({addlot: true})
          }
  
        })
      }
  
  
    })
  
  });
  router.get('/supprimerlot', function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({error: 'there are an error'})
        throw err;
      } else {
  
        var tab = []
        this.tab = []
        for (var i = 0; i < result.listedelot.length; i++) {
  
          if (result.listedelot[i]._id != req.query.idc) {
            this.tab.push(result.listedelot[i])
          }
        }
        result.listedelot = this.tab
  
        result.save(function () {
          res.send({Supprimerlot: true})
  
        })
  
      }
    })
  })
  router.get('/getalllot', function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({error: 'there are an error'});
        throw err;
      } else {
  
        res.send(result.listedelot)
      }
    })
  });
  router.get('/updatelot', function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({error: 'there are an error'})
        throw err;
      } else {
  
        var tab = [];
        this.tab = [];
        for (var i = 0; i < result.listedelot.length; i++) {
  
          if (result.listedelot[i]._id != req.query.idc) {
            this.tab.push(result.listedelot[i])
          } else {
            result.listedelot[i].nomlot = req.query.nomlot;
            result.listedelot[i].datedebut = req.query.datedebut;
            result.listedelot[i].datefin = req.query.datefin;
            this.tab.push(result.listedelot[i])
  
          }
        }
        result.listedelot = this.tab;
  
        result.save(function () {
          res.send({Updatelot: true})
  
        })
  
      }
    })
  })
  router.get("/ajoutertache", function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        for (var i = 0; i < result.listedelot.length; i++) {
          if (result.listedelot[i]._id == req.query.idc) {
            var ta = new tachemodel({
              title: req.query.title,
              start: req.query.start,
              end: req.query.end
            });
            result.listedelot[i].listedetache.push(ta);
            result.save(function (err) {
              if (err) {
                res.send({err: "there are an error"})
                throw err
              } else {
                res.send({addtache: true})
              }
  
            })
          }
  
  
        }
      }
  
    })
  
  })
  router.get("/supprimertache", function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        var tab = []
        this.tab = []
        for (var i = 0; i < result.listedelot.length; i++) {
          if (result.listedelot[i]._id == req.query.idc) {
            for (var j = 0; j < result.listedelot[i].listedetache.length; j++) {
              if (result.listedelot[i].listedetache[j]._id != req.query.idcc) {
                this.tab.push(result.listedelot[i].listedetache[j])
              }
            }
            result.listedelot[i].listedetache = this.tab
          }
        }
        result.save(function (err) {
          if (err) {
            res.send({err: "there are an error"})
            throw err
          } else {
            res.send({supprimertache: true})
          }
  
        })
      }
  
    })
  
  })
  router.get("/getalltache", function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        for (var i = 0; i < result.listedelot.length; i++) {
          if (result.listedelot[i]._id == req.query.idc) {
            res.send(result.listedelot[i].listedetache)
          }
        }
      }
  
    })
  
  })
  router.get("/updatetache", function (req, res) {
    projetmodel.findById(req.query.id, function (err, result) {
      if (err) {
        res.send({err: "there are an error"})
        throw err
      } else {
        var tab = []
        this.tab = []
        for (var i = 0; i < result.listedelot.length; i++) {
          if (result.listedelot[i]._id == req.query.idc) {
            for (var j = 0; j < result.listedelot[i].listedetache.length; j++) {
              if (result.listedelot[i].listedetache[j]._id != req.query.idcc) {
                this.tab.push(result.listedelot[i].listedetache[j])
              } else {
                result.listedelot[i].listedetache[j].title = req.query.title;
                result.listedelot[i].listedetache[j].start = req.query.start;
                result.listedelot[i].listedetache[j].end = req.query.end;
                this.tab.push(result.listedelot[i].listedetache[j]);
              }
            }
            result.listedelot[i].listedetache = this.tab
          }
        }
        result.save(function (err) {
          if (err) {
            res.send({err: "there are an error"})
            throw err
          } else {
            res.send({updatetache: true})
          }
  
        })
      }
  
    })
  
  })

router.post('/:_id/addtache',(req, res) => {
  
  var projetId=req.params._id;
  var tache = new tachemodel(req.query);
  tache.project_id=projetId;
  console.log(tache);
  
  tache.save(function (error) {
    if (error) {
      console.log(error)
    }
    res.send({
      success: true,
      message: 'tache saved successfully!'
    })
  })
})
router.post('/:_id/addlot',(req, res) => {
  var projetId=req.params._id;
  var lot = new lotmodel(req.query);
  lot.project_id=projetId;
  console.log(lot);
  lot.save(function (error) {
    if (error) {
      console.log(error)
    }
    res.send({
      success: true,
      message: 'lot saved successfully!'
    })
  })
})
  module.exports = router;
