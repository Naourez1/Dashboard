const express = require('express');
const router = express();
const tachesController = require('../controllers/taches');

router.post('/add_taches/:ids',tachesController.add_taches)
router.put('/update_taches/:id',tachesController.update_tache)
router.delete('/delete_tache/:id/:ids',tachesController.delete_tache)
router.get('/one_t/:id',tachesController.one_tache)
module.exports = router;
