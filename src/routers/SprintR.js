const express = require('express');
const router = express();
const sprintsController = require('../controllers/Sprints');

router.post('/add_sprint/:idm',sprintsController.add_sprint)
router.put('/update_sprint/:id',sprintsController.update_module)
router.get('/one_s/:id',sprintsController.one_s)
router.delete('/delete_s/:id/:idm',sprintsController.delete_s)



module.exports = router;
