const express = require('express');
const router = express();
const modulesController = require('../controllers/modules');

router.post('/add_modules/:idp',modulesController.add_modules)
router.put('/update_modules/:id',modulesController.update_module)
router.get('/one_modles/:id',modulesController.module_by_id)
router.delete('/delete_module/:id/:idp',modulesController.delete_m)


module.exports = router;
