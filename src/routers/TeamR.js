const express = require('express');
const router = express();
const teamController = require('../controllers/Team');
router.post('/add_team',teamController.add_team)
router.put('/update_team/:idt',teamController.update_team)
router.get('/supp_team/:idt/:idempl',teamController.supp_team)
router.get('/one_t/:id',teamController.one_team)
router.get('/add_empl/:idt/:idempl',teamController.add_employee)
router.get('/all_t',teamController.all_team)
module.exports = router;
