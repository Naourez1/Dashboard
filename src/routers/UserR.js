const express = require('express');
const router = express();
const userController = require('../controllers/User');
const upload=require('../../upload')



router.get('/getuser_byid/:id',userController.userbyid)

router.get('/managers',userController.list_manager)

router.get('/chefs_de_projet',userController.list_chef_de_projet)

router.get('/employees',userController.list_employer)

router.get('/accept_user/:id',userController.accept_user)

router.get('/refuse_user/:id',userController.refuse_user)

router.post('/adduser',userController.creat)

router.post('/add_pic/:id',upload.single('image'),userController.addphoto)

router.post('/login',userController.authenticate)

router.put('/updateuser/:id',userController.update)

router.delete('/delete_user/:id',userController.deleteuser)




























module.exports = router;
