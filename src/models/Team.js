var mongoose = require("mongoose");

var SChema = mongoose.Schema;

var teamSChema = new SChema({
  team_name: {
    type: String
  },
  list_employees:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'User'},
  ],
},
  {
    timestamps : true ,  usePushEach: true
  });

var team = mongoose.model("teams", teamSChema);

module.exports = team;
