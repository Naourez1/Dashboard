var mongoose = require("mongoose");
var bcrypt=require('bcrypt')
const SALT_WORK_FACTOR = 10;
var SChema = mongoose.Schema;

var usersc = new SChema({
  firstname:{
    type:String
  },
  Lastname:{
    type:String
  },
  Mail:{
    type:String,
    unique:true
  },
  Username:{
    type:String
  },
  password:{
    type:String
  },
  image:{
    type:String
  },
  Manager:{
    type:mongoose.Schema.Types.ObjectId,
    ref:'User'},
  etat:{
    type:String,
    default:0
  },
  role:{
    type:String
  },
  teams:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'team'},
  ],
  RelatedProjects:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'projet'},
  ]

},
  {
    timestamps : true ,  usePushEach: true
  }
);
usersc.pre('save', function(next) {
  var user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified('password')) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
    if (err) return next(err);

    // hash the password along with our new salt
    bcrypt.hash(user.password, salt, function(err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});


var usermodel = mongoose.model("User", usersc);

module.exports = usermodel;
