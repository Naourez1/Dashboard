var mongoose = require("mongoose");

var SChema = mongoose.Schema;
var estimation = new SChema({
  week_nb:{
    type:Number
  },
  days_nb:{
    type:Number
  },
  hours_nb:{
    type:Number
  }
})
var modulesmodelSChema = new SChema({
  title:{type:String},
  priority:{
    type:String
  },
  start_day:{
    type:Date
  },
  end_day:{
    type:Date
  },
  descreption:{type:String},
  estimation:estimation,
  Sprints:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'Sprints'
    },
  ],


})
var modulesmodel = mongoose.model("modules", modulesmodelSChema);

module.exports = modulesmodel;

