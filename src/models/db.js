var mongoose = require("mongoose");

var mongoDB = "mongodb://localhost:27017/projet-manager";

mongoose.connect(mongoDB,{ useNewUrlParser: true,useCreateIndex: true  } );

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on("error", console.error.bind(console, "MongoDB connection error:"));

