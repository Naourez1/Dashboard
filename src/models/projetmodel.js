var mongoose = require("mongoose");

var SChema = mongoose.Schema;
var projetmodelSChema = new SChema({
  nomprojet: {
    type: String
  },
  datedebut: {
    type: Date
  },
  datefin: {
    type: Date
  },
  nomclient: {
    type: String
  },
  manager:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'User'},
  ],
  chef_projet:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'User'},
  ],
  teams:
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'teams'},

  priority: {
    type: String
  },
  estimation:{
    type:'string'
  },
  descreption:{
    type:String
  },
  Modules:[
    {
      type:mongoose.Schema.Types.ObjectId,
      ref:'modules'},
  ],






},
  {
    timestamps : true ,  usePushEach: true
  });

var projetmodel = mongoose.model("Projet", projetmodelSChema);

module.exports = projetmodel;
