var mongoose = require("mongoose");

var SChema = mongoose.Schema;
var estimation = new SChema({
  week_nb:{
    type:Number
  },
  days_nb:{
    type:Number
  },
  hours_nb:{
    type:Number
  }
})

var tachesmodelSChema = new SChema({
  title:{
    type:String
  },
  priority:{
    type:String
  },
  start_day:{
    type:Date
  },
  end_day:{
    type:Date
  },

  estimation:estimation


},
  {
    timestamp:true
  })
var tachesmodel = mongoose.model("taches", tachesmodelSChema);

module.exports = tachesmodel;
