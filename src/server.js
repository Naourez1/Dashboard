var express = require("express");
// var moment = require('moment');
// moment().format('yyyy-mm-dd');
var cors = require("cors");
const bodyParser = require('body-parser');

var pro = require("./routers/ProjetR");
var user = require("./routers/UserR");
var team = require("./routers/TeamR");
var modules = require("./routers/ModulesR");
var tache = require("./routers/taches");
var sprint = require("./routers/SprintR");
var app = express();
var db=require('./models/db')

app.use(bodyParser.json({limit: '10mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}))

app.use(cors());

app.use('/uploads', express.static('uploads'));
app.use("/projet", pro);
app.use("/user", user);
app.use("/team", team);
app.use("/modules", modules);
app.use("/taches", tache);
app.use("/sprints", sprint);



app.listen(3000, function () {
  console.log("start")
});
